#language:pt

Funcionalidade: Gerenciar usuário
  Como usuário que possui conta na loja
  Quero preencher meus dados no cadastro
  Para ter acesso a utilizar a loja online


  Cenario: Cadastrar usuario
    Dado que eu tenha preenchido os dados do usuario corretamente na "inclusao"
    Quando solicitar para enviar a requisicao
    Entao a api deve retornar uma mensagem de sucesso e um id


  Cenario: Consultar usuario pelo id
     Dado que eu tenha um usuario cadastrado
     Quando realizar a consulta do usuario pelo id
     Entao a api deve retornar os dados do usuario com sucesso


#   Cenario: Alterar usuario
#     Dado que eu tenha preenchido os dados do usuario corretamente na "alteracao"
#     Quando solicitar para enviar a requisicao alterada
#     Entao a api deve retornar os dados do usuario alterados


#  Cenario: Excluir usuario
#    Dado que eu informe o id de um usuario cadastrado
#    Quando solicitar a exclusao deste usuario
#    Entao a api deve excluir o usuario e retornar uma mensagem de sucesso