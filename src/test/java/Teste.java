import com.github.javafaker.Faker;
import org.junit.Test;

import java.util.Locale;

public class Teste {

    @Test
    public void imprimirNome(){
        Faker faker = new Faker();

        String nome = String.valueOf(faker.name().firstName());
        String sobrenome = String.valueOf(faker.name().lastName());
        String nomeMaiusculo = nome + "." + sobrenome;
        String nomeCompleto = nomeMaiusculo.toLowerCase(Locale.ROOT);
        String email = String.valueOf(faker.internet().emailAddress(nomeCompleto));

        System.out.println(nome);
        System.out.println(sobrenome);
        System.out.println(email);
    }
}
