package runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

@CucumberOptions(
        features = "src/test/features/",
        glue = "steps",
        //tags = "@cadastrar and @consultar",
        stepNotifications = true,
        plugin = {"pretty", "html:target/report.html", "json:target/report.json"}
)
public class RunnerTest {

}
