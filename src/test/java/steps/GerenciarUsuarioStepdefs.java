package steps;

import core.Dados;
import core.Usuario;
import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import java.util.HashMap;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;


public class GerenciarUsuarioStepdefs {

    //private ValidatableResponse json;
    private RequestSpecification request;
    private Response response;
    String baseURI = "https://serverest.dev";
    HashMap<String, String> endpoints = new HashMap<>();
    static String id_usuario = new String();


    @BeforeStep
    public void inicializaEndpoints() {
        endpoints.put("cadastarUsuario", this.baseURI + "/usuarios");
        endpoints.put("informarUsuarioId", this.baseURI + "/usuarios/" + id_usuario);
    }
//***********************************************************************************************

    //Cadastrar usuario

    @Dado("^que eu tenha preenchido os dados do usuario corretamente na \"([^\"]*)\"$")
    public void queEuTenhaPreenchidoOsDadosDoUsuarioCorretamente(String opcao) {
        Dados dadosValidos = new Dados();
        request = given()
                .relaxedHTTPSValidation()
                .contentType(ContentType.JSON);
        if (opcao.equals("inclusao")) {
            request.body(dadosValidos.inclusaoValida());
        } else {
            request.body(dadosValidos.edicaoValida());
        }
    }

    @Quando("solicitar para enviar a requisicao")
    public void solicitarParaEnviarARequisicao() {
        response = request.when()
                .post(this.endpoints.get("cadastarUsuario"));
    }


    @Entao("a api deve retornar uma mensagem de sucesso e um id")
    public void aApiDeveRetornarUmaMensagemDeSucessoEUmId() {
        response.then()
                .log().ifValidationFails()
                .statusCode(201)
                .body("message", Matchers.is("Cadastro realizado com sucesso"))
                .body("_id", Matchers.notNullValue())
                .extract().response();
        String id = response.body().jsonPath().get("_id");
        id_usuario = id;
        System.out.println("Cadastrar usuário:");
        response.body().print();
    }

    //***********************************************************************************************

    //Consultar usuario pelo id
    @Dado("que eu tenha um usuario cadastrado")
    public void queEuTenhaUmUsuarioCadastrado() {
        request = given()
                .relaxedHTTPSValidation()
                .contentType(ContentType.JSON);
    }

    @Quando("realizar a consulta do usuario pelo id")
    public void realizarAConsultaDoUsuarioPeloId() {
        response = request.when()
                .get(this.endpoints.get("informarUsuarioId"));
    }

    @Entao("a api deve retornar os dados do usuario com sucesso")
    public void aApiDeveRetornarOsDadosDoUsuarioComSucesso() {
        response.then()
                .log().ifValidationFails()
                .statusCode(200)
                .body("nome", Matchers.notNullValue())
                .body("_id", Matchers.equalTo(id_usuario))
                .extract().response();
        System.out.println("Consultar usuario pelo id:");
        response.body().print();
    }

    //***********************************************************************************************

    //Alterar usuario pelo id

    @Quando("solicitar para enviar a requisicao alterada")
    public void solicitarParaEnviarARequisicaoAlterada() {
        response = request.when()
                .put(this.endpoints.get("informarUsuarioId"));
    }

    @Entao("a api deve retornar os dados do usuario alterados")
    public void aApiDeveRetornarOsDadosDoUsuarioAlterados() {
        response.then()
                .log().ifValidationFails()
                .statusCode(200)
                .body("message", Matchers.is("Registro alterado com sucesso"))
                .extract().response();
        System.out.println("Alterar usuario pelo id:");
        response.body().print();
    }

    //***********************************************************************************************
    //Excluir usuario pelo id

    @Dado("que eu informe o id de um usuario cadastrado")
    public void que_eu_informe_id_usuario_cadastrado() {
        request = given()
                .relaxedHTTPSValidation()
                .contentType(ContentType.JSON);
    }

    @Quando("solicitar a exclusao deste usuario")
    public void solicitar_a_exclusao_deste_usuario() {
        response = request.when()
                .delete(this.endpoints.get("informarUsuarioId"));
    }

    @Entao("a api deve excluir o usuario e retornar uma mensagem de sucesso")
    public void a_api_deve_excluir_o_usuario_e_retornar_uma_mensagem_de_sucesso() {
        response.then()
                .log().ifValidationFails()
                .statusCode(200)
                .body("message", Matchers.is("Registro excluído com sucesso"));

    }
}
