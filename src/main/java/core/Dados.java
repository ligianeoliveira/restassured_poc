package core;

import com.github.javafaker.Faker;

import java.util.Locale;

public class Dados {

    public Usuario inclusaoValida() {
        Faker faker = new Faker();

        String nome = String.valueOf(faker.name().firstName());
        String sobrenome = String.valueOf(faker.name().lastName());
        String nomeCompleto = nome + " "  + sobrenome;
        String email = String.valueOf(faker.internet().emailAddress(nome.toLowerCase(Locale.ROOT) + "." + sobrenome.toLowerCase(Locale.ROOT)));
        String password = String.valueOf(faker.internet().password());

        final Usuario usuario = new Usuario();
        usuario.setNome(nomeCompleto);
        usuario.setEmail(email);
        usuario.setPassword(password);
        usuario.setAdministrador("true");

        return usuario;
    }

    public Usuario edicaoValida() {
        Faker faker = new Faker();

        String nomeCompleto = "Ligiane";
        String email = "teste@testeqa.com";
        String password = String.valueOf(faker.internet().password());

        final Usuario usuario = new Usuario();
        usuario.setNome(nomeCompleto);
        usuario.setEmail(email);
        usuario.setPassword(password);
        usuario.setAdministrador("true");

        return usuario;
    }

}
